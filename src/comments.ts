/* eslint-disable @typescript-eslint/naming-convention */
import express, { type Request, type Response, type RequestHandler } from "express";
import { type Comment } from "./types";
import { addComment, deleteComment, getComments } from "./dao";

const comments = express.Router();

// `GET /comments/:userID` Lists all comments by the user defined by the userId parameter
comments.get("/:userID", (async (req: Request, res: Response) => {
    const { userID } = req.params;
    const post = await getComments(Number(userID));
    res.send(post);
}) as RequestHandler);

// `POST /comments` Adds a new comment
comments.post("/", (async (req: Request, res: Response) => {
    const { user_id, post_id, comment_date, content } = req.body as Comment;
    const post = await addComment(user_id, post_id, comment_date, content);
    res.status(201).send(post);
}) as RequestHandler);

// `DELETE /comments/:id` Deletes the post
comments.delete("/:id", (async (req: Request, res: Response) => {
    const { id } = req.params;
    const comment = await deleteComment(Number(id));
    if(comment?.length) {
        res.status(204).send();
    } else {
        res.status(404).send();
    }
}) as RequestHandler);

export default comments;
