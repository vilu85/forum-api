/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express, { type Request, type Response } from "express";
import { addUser, deleteUser, getAllUsers, getUser } from "./dao";

import { type User } from "./types";

const users = express.Router();

// `GET /users` Lists the id's and usernames of all users.
users.get("/", async (_req: Request, res: Response) => {
    const users = await getAllUsers();
    return res.send(users);
});

// `GET /users/:id` Gives detailed user information
users.get("/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const user = await getUser(Number(id));
    res.send(user);
});

// `POST /users` Adds a new user
users.post("/", async (req: Request<User>, res: Response) => {
    const { username, email, fullname, pwhash } = req.body as User;
    const id = await addUser(username, fullname, email, pwhash);
    res.status(201).send({ username, email, fullname, id });
});

// `DELETE /users/:id` Deletes the post
users.delete("/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const user = await deleteUser(Number(id));
    if(user?.length) {
        res.status(204).send();
    } else {
        res.status(404).send();
    }
});

export default users;
