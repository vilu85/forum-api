import express from "express";
import users from "./users";
import posts from "./posts";
import comments from "./comments";

const server = express();
server.use(express.json());

server.use("/users", users);
server.use("/posts", posts);
server.use("/comments", comments);

export default server;
