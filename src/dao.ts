import { executeQuery } from "./db";
import { type Comment, type Post, type User } from "./types";

export const addUser = async (
    username: string,
    fullName: string,
    email: string,
    passwordHash?: string
): Promise<number> => {
    const query = "INSERT INTO users (username, fullname, email, pwhash) VALUES ($1, $2, $3, $4) RETURNING id;";
    const params = [username, fullName, email, passwordHash ?? ""];
    const result = await executeQuery<User>(query, params);
    return result.rows[0].id;
};

export const getUser = async (id: number): Promise<User> => {
    const query = "SELECT * FROM users WHERE id = $1";
    const params = [id];
    const result = await executeQuery<User>(query, params);
    return result.rows[0];
};

// /users` Lists the id's and usernames of all users.
export const getAllUsers = async (): Promise<User[]> => {
    const query = "SELECT users.id, users.username FROM users;";
    const params = [];
    const result = await executeQuery<User>(query, params);
    return result.rows;
};

//  `GET /posts` Lists all post id's, poster id's and titles
export const getAllPosts = async (): Promise<Post[]> => {
    const query = "SELECT posts.id, posts.user_id, posts.title from posts;";
    const params = [];
    const result = await executeQuery<Post>(query, params);
    return result.rows;
};

// Fetch detailed information of the post, including all the comments associated with the post.
export const getPost = async (id: number): Promise<Post[]> => {
    const query = `SELECT json_build_object(
        'id', posts.id,
        'user_id', posts.user_id,
        'title', posts.title,
        'content', posts.content,
        'post_date', posts.post_date,
        'comments', COALESCE(
            json_agg(
                json_build_object(
                    'id', comments.id,
                    'user_id', comments.user_id,
                    'post_id', comments.post_id,
                    'content', comments.content,
                    'comment_date', comments.comment_date
                )
            )
            FILTER (
                WHERE comments.id IS NOT NULL
            )
        )
    ) AS post
    FROM posts 
    LEFT JOIN comments ON posts.id = comments.post_id
    WHERE posts.id = $1
    GROUP BY posts.id`;
  
    const params = [id];
    const result = await executeQuery<Post[]>(query, params);

    if (result.rows.length === 0) {
        return null;
    }

    return result.rows[0];
};

export const addPost = async (userId: number, title: string, content: string, postDate: string): Promise<Post> => {
    const query = "INSERT INTO posts (user_id, post_date, title, content) VALUES ($1, $2, $3, $4) RETURNING *;";
    const params = [userId, postDate, title, content];
    const result = await executeQuery<Post>(query, params);
    return result.rows[0];
};

export const getComments = async (userId: number): Promise<Post[]> => {
    const query = "SELECT * from comments where user_id = $1;";
    const params = [userId];
    const result = await executeQuery<Post>(query, params);
    return result.rows;
};

export const addComment = async (userId: number, postId: number, commentDate: string, content: string): Promise<Comment> => {
    const query = "INSERT INTO comments (user_id, post_id, content, comment_date) VALUES ($1, $2, $3, $4) RETURNING *;";
    const params = [userId, postId, content, commentDate];
    const result = await executeQuery<Comment>(query, params);
    return result.rows[0];
};

export const deletePost = async (id: number): Promise<Post[]> => {
    const query = "DELETE FROM posts WHERE id = $1 RETURNING *;";
    const params = [id];
    const result = await executeQuery<Post>(query, params);
    return result.rows;
};

export const deleteComment = async (id: number): Promise<Comment[]> => {
    const query = "DELETE FROM comments WHERE id = $1 RETURNING *;";
    const params = [id];
    const result = await executeQuery<Comment>(query, params);
    return result.rows;
};

export const deleteUser = async (id: number): Promise<User[]> => {
    const query = "DELETE FROM users WHERE id = $1 RETURNING *;";
    const params = [id];
    const result = await executeQuery<User>(query, params);
    return result.rows;
};
