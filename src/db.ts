// db.ts
import pg from "pg";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, PG_ENV } = process.env;

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: PG_ENV === "production"
});

/* eslint-disable */
export const executeQuery = async <T>(query: string, parameters?: Array<any>): Promise<{ rows: T[]}> => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result as { rows: T[] };
    } catch (error: any) {
        console.error(error.stack);
        error.name = "dbError";
        throw error;
    } finally {
        client.release();
    }
};
/* eslint-enable */
