export interface User {
    id?: number;
    username: string;
    fullname: string;
    email: string;
    pwhash: string;
}

export interface Post {
    id?: number;
    user_id: number;
    title: string;
    content: string;
    post_date: string;
}
  
export interface Comment {
    id?: number;
    user_id: number;
    post_id: number;
    content: string;
    comment_date: string;
}
