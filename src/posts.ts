/* eslint-disable @typescript-eslint/naming-convention */
import express, { type Request, type Response, type RequestHandler } from "express";
import { type Post } from "./types";
import { addPost, deletePost, getAllPosts, getPost } from "./dao";

const posts = express.Router();

// `GET /posts` Lists all post id's, poster id's and titles
posts.get("/", (async (_req: Request, res: Response) => {
    const posts = await getAllPosts();
    return res.send(posts);
}) as RequestHandler);

// `GET /posts/:id` Sends detailed information of the post, including all the comments associated with the post.
posts.get("/:id", (async (req: Request, res: Response) => {
    const { id } = req.params;
    const post = await getPost(Number(id));
    res.send(post);
}) as RequestHandler);

// `POST /posts` Adds a new post
posts.post("/", (async (req: Request, res: Response) => {
    const { user_id, title, content, post_date } = req.body as Post;
    const post = await addPost(user_id, title, content, post_date);
    res.status(201).send(post);
}) as RequestHandler);

// `DELETE /posts/:id` Deletes the post
posts.delete("/:id", (async (req: Request, res: Response) => {
    const { id } = req.params;
    const post = await deletePost(Number(id));
    if(post?.length) {
        res.status(204).send();
    } else {
        res.status(404).send();
    }
}) as RequestHandler);

export default posts;
