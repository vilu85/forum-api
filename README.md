## Assignment 13.10: Forum Pipeline

Create a Gitlab CI/CD pipeline that lints, tests, and deploys the Forum API to Azure when new code is pushed.