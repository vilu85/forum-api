import { pool } from "../src/db";
import request from "supertest";
import server from "../src/server";

// Mock the pool methods.
const initializeMockPool = (mockResponse: unknown): void => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe("Testing /comments endpoint", () => {
    const mockResponse = {
        rows: [
            {
                id: 1,
                user_id: 1,
                post_id: 1,
                content: "Test comment 1",
                comment_date: "2023-05-24T21:00:00.000Z"
            },
            {
                id: 2,
                user_id: 1,
                post_id: 2,
                content: "Test comment 2",
                comment_date: "2023-06-14T21:00:00.000Z"
            }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("GET /comments/:id returns all comments by user id", async () => {
        const response = await request(server).get("/comments/1")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows);
    })

    it("POST /comments adds the new comment", async () => {
        const mockComment = { user_id: 2, post_id: 2, content: "Test comment 3", comment_date: "2023-06-17T21:00:00.000Z" };
        const mockNewPostResponse = {
            rows: [
                {
                    ...mockComment
                }
            ]
        };

        initializeMockPool(mockNewPostResponse);
        const response = await request(server).post("/comments").send(mockComment).set("Accept", "application/json");
        expect(response.statusCode).toBe(201)
        expect(response.body).toStrictEqual(mockComment);
    })

    it("DELETE /comments/:id returns a status code 204", async () => {
        const response = await request(server).delete("/comments/2").send();
        expect(response.statusCode).toBe(204)
    })
})
