import { pool } from "../src/db";
import request from "supertest";
import server from "../src/server";

// Mock the pool methods.
const initializeMockPool = (mockResponse: unknown): void => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe("Testing /users endpoint", () => {
    const mockResponse = {
        rows: [
            { username: "testuser1", fullname: "Test user 1", email: "test@user1.com", id: 1, pwhash: "12345" },
            { username: "testuser2", fullname: "Test user 2", email: "test@user2.com", id: 2, pwhash: "23456" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("GET /users returns all users", async () => {
        const response = await request(server).get("/users")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows);
    })

    it("GET /users/:id returns the user by id", async () => {
        const response = await request(server).get("/users/1")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows[0]);
    })

    it("POST /users adds new user", async () => {
        const mockUser = { username: "testuser3", fullname: "Test user 3", email: "test@user3.com", id: 1 };
        const response = await request(server).post("/users").send(mockUser).set("Accept", "application/json");
        expect(response.statusCode).toBe(201)
        expect(response.body).toStrictEqual(mockUser);
    })

    it("DELETE /users/:id returns a status code 204", async () => {
        const response = await request(server).delete("/users/3").send();
        expect(response.statusCode).toBe(204)
    })
})
