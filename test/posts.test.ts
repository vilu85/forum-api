import { pool } from "../src/db";
import request from "supertest";
import server from "../src/server";
import { type Post } from "../src/types";

// Mock the pool methods.
const initializeMockPool = (mockResponse: unknown): void => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe("Testing /posts endpoint", () => {
    const mockResponse = {
        rows: [
            {
                id: 1,
                user_id: 1,
                title: "Test post"
            },
            {
                id: 2,
                user_id: 1,
                title: "Another test"
            },
            {
                id: 3,
                user_id: 3,
                title: "Third test"
            }
        ]
    }

    beforeEach(() => {
        initializeMockPool(mockResponse)
    })

    afterEach(() => {
        jest.clearAllMocks()
    })

    it("GET /posts returns all posts", async () => {
        const response = await request(server).get("/posts")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows);
    })

    it("GET /posts/:id returns the post by id", async () => {
        const response = await request(server).get("/posts/1")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows[0]);
    })

    it("POST /posts adds a new post", async () => {
        const mockPost: Post = {
            id: 15,
            user_id: 2,
            title: "Mock title",
            content: "Mock content",
            post_date: "2023-06-15T21:00:00.000Z"
        };
        const mockNewPostResponse = {
            rows: [
                {
                    ...mockPost
                }
            ]
        };

        initializeMockPool(mockNewPostResponse);
        const response = await request(server).post("/posts").send(mockPost).set("Accept", "application/json");
        expect(response.statusCode).toBe(201)
        expect(response.body.title).toBe(mockPost.title);
        expect(response.body).toStrictEqual(mockPost);
    })

    it("DELETE /posts/:id returns the status code 204", async () => {
        const response = await request(server).delete("/posts/1")
        expect(response.statusCode).toBe(204)
    })
})
